﻿#define DEBUG_KEYS

// Unity Framework
using FrameLord.Input;
using UnityEngine;

namespace SpaceInvaders
{
    public static class GameInput
    {
        public static bool GetActionLeft()
        {
                return Input.GetKey(KeyCode.LeftArrow) || Input.GetAxis("Horizontal") < -0.1f;
        }

        public static bool GetActionRight()
        {
            return Input.GetKey(KeyCode.RightArrow) || Input.GetAxis("Horizontal") > 0.1f;
        }

        public static bool GetActionFire()
        {
            return Input.GetKey(KeyCode.Space) || Input.GetKeyDown($"joystick button {GamePad.GamepadButtonA}");
        }

        public static bool GetDebugKeyDie()
        {
#if DEBUG_KEYS
            return Input.GetKeyDown(KeyCode.D);
#else
        return false;
#endif
        }

        public static bool GetDebugRespawn()
        {
#if DEBUG_KEYS
            return Input.GetKeyDown(KeyCode.R);
#else
        return false;
#endif
        }

        public static bool GetDebugKillemall()
        {
#if DEBUG_KEYS
            return Input.GetKeyDown(KeyCode.K);
#else
        return false;
#endif
        }

        public static bool GetDebugShipBoss()
        {
#if DEBUG_KEYS
            return Input.GetKeyDown(KeyCode.B);
#else
        return false;
#endif
        }
    }
}