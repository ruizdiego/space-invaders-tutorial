﻿using System.Collections;
using System.Collections.Generic;
using FrameLord.Sound;
using UnityEngine;

public class SoundManagerTester : MonoBehaviour
{
    private int chnId; 
    
    // Start is called before the first frame update
    void Start()
    {
        SoundManager.Instance.PlaySound("MainMenuMusic");
        /*chnId = SoundManager.Instance.FadeInSound("MainMenuMusic", 3f, () =>
        {
            Debug.Log("Verdura");
            SoundManager.Instance.FadeOutSound(chnId, 5f);
        });*/
    }

    void OnGUI()
    {
        if (GUILayout.Button("Pause"))
        {
            //SoundManager.Instance.SetVolume("Music", 0.5f);
            SoundManager.Instance.PauseAllSounds();
        }
        
        if (GUILayout.Button("Resume"))
        {
            //SoundManager.Instance.SetVolume("Music", 0.5f);
            SoundManager.Instance.ResumeAllSounds();
        }
        
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
