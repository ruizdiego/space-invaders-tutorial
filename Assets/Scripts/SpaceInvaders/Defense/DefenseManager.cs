﻿// Unity Framework
using UnityEngine;

// FrameLord
using FrameLord.Core;

namespace SpaceInvaders
{
    public class DefenseManager : MonoBehaviorSingleton<DefenseManager>
    {
        // Number of defenses
        public int numOfDefenses = 4;

        // Defense prefab reference
        public GameObject defensePrefab;

        // Distance from bottom screen in pixels
        public int distanceFromBottom = 100;

        public Texture2D damageTexture;

        // List of defenses
        private Defense[] defenses;

        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            CreateDefenses();
        }

        private void CreateDefenses()
        {
            var defensesSep = GameManager.ScreenWidth / (numOfDefenses + 1);
            var firstDefensePos = -(GameManager.ScreenWidth >> 1) + defensesSep;

            defenses = new Defense[numOfDefenses];

            for (int i = 0; i < numOfDefenses; i++)
            {
                var defenseGO = GameObject.Instantiate(defensePrefab, transform);
                defenseGO.transform.position = new Vector3(firstDefensePos + i * defensesSep, (float) (-(GameManager.ScreenHeight >> 1) + distanceFromBottom), 0f);
                defenses[i] = defenseGO.GetComponent<Defense>();
            }
        }

    }
}