﻿using System;

// Unity Framework
using UnityEngine;

// FrameLord
using FrameLord.Sound;

namespace SpaceInvaders
{
    public class Defense : MonoBehaviour
    {
        // Shield texture height
        private int _shieldHeight;
        
        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            CreateSpriteDefense();
        }

        /// <summary>
        /// Create a new sprite with his own cloned texture
        /// </summary>
        private void CreateSpriteDefense()
        {
            var sr = GetComponent<SpriteRenderer>();
            var tex = sr.sprite.texture;
            var newTex = new Texture2D(tex.width, tex.height, TextureFormat.RGBA32, false);
            newTex.filterMode = FilterMode.Point;
            newTex.SetPixels32(tex.GetPixels32());
            newTex.Apply();

            _shieldHeight = tex.height;

            sr.sprite = Sprite.Create(newTex, new Rect(0, 0, newTex.width, newTex.height), new Vector2(0.5f, 0f), 1f);
        }

        /// <summary>
        /// Returns the relative Y position hit where the projectile impacted the defense Or -1 if
        /// the defense hasn't been hit.
        /// </summary>
        private int GetHitYPos(Texture2D tex, int x, bool isPlayer)
        {
            for (int ypos = 0; ypos < _shieldHeight; ypos++)
            {
                var yHitPos = isPlayer ? ypos : tex.height - ypos - 1;

                Color c = tex.GetPixel(x, yHitPos);

                if (c.a != 0) return yHitPos;
            }

            return -1;
        }

        /// <summary>
        /// Unity OnTriggerEnter2D Method
        /// </summary>
        private void OnTriggerEnter2D(Collider2D other)
        {
            // If a ship collides with the defense, destroy all the defense at once
            if (other.gameObject.GetComponent<Ship>() != null)
            {
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<PolygonCollider2D>().enabled = false;
                return;
            }
            
            var sr = GetComponent<SpriteRenderer>();
            var tex = sr.sprite.texture;

            var defLeftPos = transform.position.x - (tex.width >> 1);
            var pixPosX = (int) Mathf.Abs(defLeftPos - other.gameObject.transform.position.x);

            bool projectileFromPlayer = IsProjectileFromPlayer(other.gameObject);

            int pixPosY = GetHitYPos(tex, pixPosX, projectileFromPlayer);

            if (pixPosY != -1)
            {
                // Afecto la textura. El color clear es un pixel transparente sin color.
                ApplyDamageTexture(tex, pixPosX, pixPosY, projectileFromPlayer);

                // Destruyo o reciclo el proyectil
                other.gameObject.GetComponent<Projectile>()?.Destroy();
                
                // [Sound]
                SoundManager.Instance.PlaySound("ShieldImpact");
            }

        }

        /// <summary>
        /// Is the projectile from the player?
        /// </summary>
        bool IsProjectileFromPlayer(GameObject go)
        {
            return string.Compare(go.tag, "Player", StringComparison.Ordinal) == 0;
        }

        /// <summary>
        /// Apply damage to the defense in the specified position
        /// </summary>
        void ApplyDamageTexture(Texture2D tex, int posx, int posy, bool isPlayer)
        {
            var damageTex = DefenseManager.Instance.damageTexture;

            for (int x = 0; x < damageTex.width; x++)
            {
                for (int y = 0; y < damageTex.height; y++)
                {
                    var pixColor = damageTex.GetPixel(x, isPlayer ? y : damageTex.height - y - 1);

                    if (pixColor.a > 0)
                    {
                        var xHitPos = posx + x - (damageTex.width >> 1);
                        var yHitPos = posy + y - 2;

                        if (0f <= xHitPos && xHitPos < tex.width && 0f <= yHitPos && yHitPos < tex.height)
                            tex.SetPixel(xHitPos, yHitPos, Color.clear);
                    }
                }
            }

            // Apply changes to the texture
            tex.Apply();
        }
    }
}