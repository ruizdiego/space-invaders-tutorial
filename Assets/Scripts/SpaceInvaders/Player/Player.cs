﻿// .NET Framework
using System;

// Unity Framework
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace SpaceInvaders
{
    public class Player : Ship
    {
        // Tag of the player
        public const string Tag = "Player";

        // Player movement speed
        public float movSpeed = 200f;

        // Referencia a la transformación de la posición de inicio del disparo
        public Transform projectileStartPos;

        // Screen margin limit
        private float _screenMarginLimit;

        // Time accumulator used for basic anims
        private float _accumTimeAnim;

        // Initial Y position
        private float _initialPosY;
        
        /// <summary>
        /// Unity Awake Method
        /// </summary>
        new void Awake()
        {
            base.Awake();
            
            _screenMarginLimit = (GameManager.ScreenWidth >> 1) * 0.9f;

            _initialPosY = transform.position.y;
        }

        /// <summary>
        /// Unity Update Method
        /// </summary>
        private new void Update()
        {
            base.Update();

            switch (_state)
            {
                case State.Alive:

                    if (GameManager.Instance.IsPlaying())
                    {
                        if (GameInput.GetActionLeft())
                        {
                            var xpos = Mathf.Max(transform.position.x - movSpeed * Time.deltaTime, -_screenMarginLimit);
                            transform.position = new Vector3(xpos, transform.position.y, transform.position.z);
                        }
                        else if (GameInput.GetActionRight())
                        {
                            var xpos = Mathf.Min(transform.position.x + movSpeed * Time.deltaTime, _screenMarginLimit);
                            transform.position = new Vector3(xpos, transform.position.y, transform.position.z);
                        }

                        if (GameInput.GetActionFire() && CanShootAlready())
                        {
                            Shoot();
                        }

                        if (GameInput.GetDebugKeyDie())
                        {
                            Die();
                        }
                    }

                    break;

                case State.Dying:

                    _accumTimeAnim += Time.deltaTime;

                    if (_accumTimeAnim > 0.1f)
                    {
                        _accumTimeAnim %= 0.1f;
                        _sprRnd.flipX = !_sprRnd.flipX;
                    }

                    break;

                case State.Death:
                    if (GameInput.GetDebugRespawn())
                    {
                        Respawn();
                    }

                    break;
            }

        }

        public void Shoot()
        {
            Shoot(projectileStartPos.position);
        }

        public void Respawn()
        {
            transform.position = new Vector3(0f, _initialPosY, 0f);
            _state = State.Alive;
            _sprRnd.flipX = false;
            _sprRnd.enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            _sprRnd.sprite = aliveSprites[0];
        }

        
    }
}
