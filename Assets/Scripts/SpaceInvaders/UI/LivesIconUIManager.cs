﻿// .NET Framework
using System.Collections.Generic;

// Unity Framework
using UnityEngine;

// FrameLord
using FrameLord.EventDispatcher;

namespace SpaceInvaders
{
    public class LivesIconUIManager : MonoBehaviour
    {
        // The player cannot get more than this number of lives
        private const int MaxNumberOfLives = 10;

        // Player life icon prefab reference
        public GameObject playerLifeIconPrefabRef;

        // List of created icons
        private List<GameObject> livesIcons;

        // Number of visible icons
        private int numOfVisibleIcons;

        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            livesIcons = new List<GameObject>();

            SetIcons(GameManager.Instance.GetLives() - 1);
            
            GameEventDispatcher.Instance.AddListener(EvnPlayerRespawn.Name, OnPlayerRespawn);
        }

        private void OnDestroy()
        {
            GameEventDispatcher.Instance.RemoveListener(EvnPlayerRespawn.Name, OnPlayerRespawn);
        }

        /// <summary>
        /// Set the total number of lives
        /// </summary>
        public void SetIcons(int visibleIcons)
        {
            for (int i = 0; i < MaxNumberOfLives; i++)
            {
                var go = GameObject.Instantiate(playerLifeIconPrefabRef, transform);
                livesIcons.Add(go);

                // Disable the go if it's greater than the number of visible ones
                if (i >= visibleIcons) go.SetActive(false);
            }

            numOfVisibleIcons = visibleIcons;
        }

        /// <summary>
        /// Hide the last visible icon
        /// </summary>
        public void HideLastOne()
        {
            if (numOfVisibleIcons > 0)
            {
                numOfVisibleIcons--;
                livesIcons[numOfVisibleIcons].SetActive(false);
            }
        }

        /// <summary>
        /// Show one more icon
        /// </summary>
        public void ShowOneMore()
        {
            if (numOfVisibleIcons < MaxNumberOfLives)
            {
                livesIcons[numOfVisibleIcons].SetActive(true);
                numOfVisibleIcons++;
            }
        }

        private void OnPlayerRespawn(System.Object sender, GameEvent e)
        {
            HideLastOne();
        }
    }
}
