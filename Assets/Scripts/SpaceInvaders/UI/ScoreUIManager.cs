﻿using System;
using System.Collections;
using System.Collections.Generic;

// Unity Framework
using UnityEngine;
using UnityEngine.UI;

// FrameLord
using FrameLord.EventDispatcher;

namespace SpaceInvaders
{
    public class ScoreUIManager : MonoBehaviour
    {
        // Player score text reference
        public Text playerScore;

        // Hi score text reference
        public Text hiScore;

        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            playerScore.text = $"{GameManager.Instance.score:D4}";
            hiScore.text = $"{HiscoreManager.hiscore:D4}";
            
            GameEventDispatcher.Instance.AddListener(EvnScoreUpdate.Name, OnScoreUpdate);
        }

        private void OnDestroy()
        {
            GameEventDispatcher.Instance.RemoveListener(EvnScoreUpdate.Name, OnScoreUpdate);
        }

        public void AddPlayerScore(int valueToAdd)
        {
            GameManager.Instance.score += valueToAdd;
            playerScore.text = $"{GameManager.Instance.score:D4}";

            if (GameManager.Instance.score > HiscoreManager.hiscore)
            {
                HiscoreManager.hiscore = GameManager.Instance.score;
                hiScore.text = $"{HiscoreManager.hiscore:D4}";
            }
        }

        private void OnScoreUpdate(System.Object sender, GameEvent e)
        {
            AddPlayerScore(((EvnScoreUpdate) e).scoreIncrementValue);
        }
    }
}