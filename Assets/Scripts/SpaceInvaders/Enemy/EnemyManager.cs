﻿// Mono Framework
using System.Collections.Generic;
using FrameLord.Core;
using FrameLord.Sound;

// Unity Framework
using UnityEngine;

namespace SpaceInvaders
{
    public class EnemyManager : MonoBehaviorSingleton<EnemyManager>
    {
        // Array de referencias de prefabs de enemigos 
        public GameObject[] enemyTypePrefab;

        // Colores de los enemigos
        public Color[] enemyColors;

        // Cantidad de enemigos a instanciar por línea
        public int enemyPerRow = 11;
        
        // Acumulador de tiempo utilizado para el movimiento de las naves
        private float _accumTime;

        // Velocidad de avance de las naves Cada vez que el tiempo acumulador alcance este
        // número, las naves se moveran un paso
        private float _timeMovementStep = 1f;

        // Listado de enemigos instanciados
        private List<GameObject>[] _lstEnemy;

        // Dirección de movimiento (1: derecha, -1: izquierda)
        private float _movDir = 1f;

        // Posición máxima X
        private float _maxPosX;

        // Salto en Y de la nave cuando cambia de dirección
        private float _movDeltaY = 0f;

        // Acumulador de tiempo utilizado para espaciar los disparos enemigos
        private float _accumTimeShooting;

        // Mínima posición de la nave en X e Y
        private Vector3 _minPos;

        // Máxima posición de la nave en X e Y
        private Vector3 _maxPos;

        // Flag to choose the proper moving sound
        private bool _flipFlop = false;

        private float _shootStep;
        
        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            Debug.AssertFormat(enemyTypePrefab != null, "enemyTypePrefab cannot be null");

            _lstEnemy = new List<GameObject>[enemyTypePrefab.Length];
            
            CreateEnemyShips();

            _accumTime = 0f;

            SetLevelDifficulty();
        }

        /// <summary>
        /// Unity Update Method
        /// </summary>
        void Update()
        {
            if (GameManager.Instance.IsPlaying())
            {
                _accumTime += Time.deltaTime;

                // 1. Lógica de movimiento de naves enemigas
                if (_accumTime > _timeMovementStep)
                {
                    MoveEnemies();
                    _accumTime = 0f;
                }

                // 2. Lógica de disparo de naves enemigas
                _accumTimeShooting += Time.deltaTime;

                if (_accumTimeShooting >= _shootStep)
                {
                    ShootToPlayer();
                    _accumTimeShooting = 0f;
                }

                if (GameInput.GetDebugKillemall())
                {
                    KillemAll();
                }
            }
        }

        private void SetLevelDifficulty()
        {
            _timeMovementStep = Mathf.Max(1.1f - (GameManager.Instance.levelNum / 10f), 0.1f);
            _shootStep = _timeMovementStep;
        }

        private void CreateEnemyShips()
        {
            var screenWidth = GameManager.ScreenWidth;
            var screenHeight = GameManager.ScreenHeight;
            
            float xMargin = (screenWidth >> 1) * 0.9f;
            float yMargin = (screenHeight >> 1) * 0.5f;

            _minPos = new Vector3(-xMargin, yMargin, 0f);
            _maxPos = new Vector3(xMargin, -yMargin, 0f);
            float sepX = (_maxPos.x - _minPos.x) / (enemyPerRow + 1);
            float sepY = (_maxPos.y - _minPos.y) / (enemyTypePrefab.Length + 1);
            
            for (int i = 0; i < enemyTypePrefab.Length; i++)
            {
                _lstEnemy[i] = new List<GameObject>();

                for (int j = 0; j < enemyPerRow; j++)
                {
                    GameObject enemy = Instantiate(enemyTypePrefab[i]);
                    enemy.name = string.Format("enemy-line-{0}-{1}", i, j);
                    enemy.transform.position = new Vector3(_minPos.x + sepX + sepX * j, _minPos.y + sepY * i, 0f);
                    enemy.transform.parent = transform;
                    enemy.GetComponent<Ship>().SetColor(enemyColors[i]);

                    _lstEnemy[i].Add(enemy);
                }
            }
        }

        void MoveEnemies()
        {
            float shipWithMinPosX = float.MaxValue;
            float shipWithMaxPosX = float.MinValue;

            const float MovDirXScale = 2f;
            const float MovDirYScale = 10f;

            for (int i = 0; i < _lstEnemy.Length; i++)
            {
                for (int j = 0; j < _lstEnemy[i].Count; j++)
                {
                    GameObject enemyGO = _lstEnemy[i][j];

                    if (enemyGO != null)
                    {
                        enemyGO.transform.position = new Vector3(enemyGO.transform.position.x + _movDir * MovDirXScale, enemyGO.transform.position.y - _movDeltaY,
                            enemyGO.transform.position.z);

                        if (enemyGO.transform.position.x < shipWithMinPosX)
                            shipWithMinPosX = enemyGO.transform.position.x;

                        if (enemyGO.transform.position.x > shipWithMaxPosX)
                            shipWithMaxPosX = enemyGO.transform.position.x;

                        enemyGO.GetComponent<Ship>().ChangeFrame();
                    }
                }
            }

            if (_movDeltaY > 0f)
                _movDeltaY = 0f;

            if (_movDir == 1f && shipWithMaxPosX > _maxPos.x)
            {
                _movDir = -1f;
                _movDeltaY = MovDirYScale;
                _timeMovementStep *= 0.9f;
            }
            else if (_movDir == -1f && shipWithMinPosX < _minPos.x)
            {
                _movDir = 1f;
                _movDeltaY = MovDirYScale;
                _timeMovementStep *= 0.9f;
            }

            TriggerSound();
        }

        void ShootToPlayer()
        {
            // 1. Elijo una columna de enemigos donde aún queden enemigos vivos
            int col = Random.Range(0, enemyPerRow);

            Vector3 shootPos = Vector3.zero;

            // 2. Selecciono la nave que se encuentre en la fila inferior de dicha columna
            // (de otro modo las naves enemigas podrían matarse entre ellas o al menos
            // quedaría raro que un disparo enemigo atraviese sus propias naves sin matarlas)
            for (int i = _lstEnemy.Length - 1; i >= 0; i--)
            {
                var ship = _lstEnemy[i][col].GetComponent<Ship>();
                if (ship.IsAlive())
                {
                    shootPos = _lstEnemy[i][col].transform.position;
                    ship.Shoot(shootPos);
                    break;
                }
            }
        }
        
        private void TriggerSound()
        {
            if (_flipFlop)
                SoundManager.Instance.PlaySound("EnemyMov1");
            else
                SoundManager.Instance.PlaySound("EnemyMov2");
        
            _flipFlop = !_flipFlop;
        }

        private void KillemAll()
        {
            for (int i = 0; i < _lstEnemy.Length; i++)
            {
                for (int col = 0; col < _lstEnemy[i].Count; col++)
                {
                    var ship = _lstEnemy[i][col].GetComponent<Ship>();
                    ship.Die();
                }
            }
        }

        public bool AreAllShipsDestroyed()
        {
            for (int i = 0; i < _lstEnemy.Length; i++)
            {
                for (int col = 0; col < _lstEnemy[i].Count; col++)
                {
                    var ship = _lstEnemy[i][col].GetComponent<Ship>();
                    if (ship.IsAlive()) return false;
                }
            }

            return true;
        }
        
    }
}