﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using FrameLord.Pool;
using SpaceInvaders;
using UnityEngine;

public class EnemyBossManager : MonoBehaviour
{
    private const float MinAppearTime = 12f;
    private const float MaxAppearTime = 20f;
    
    public GameObject enemyBossPrefab;

    private float _accumTime;

    private float _appearTime;
    
    /// <summary>
    /// Unity Start Method
    /// </summary>
    void Start()
    {
        _appearTime = Random.Range(MinAppearTime, MaxAppearTime);
    }

    /// <summary>
    /// Unity Update Method
    /// </summary>
    void Update()
    {
        if (GameManager.Instance.IsPlaying())
        {
            _accumTime += Time.deltaTime;
            if (_accumTime > _appearTime)
            {
                var pool = PoolManager.Instance.GetPool("EnemyBoss");
                var boss = pool.GetItem();
                
                if (boss != null)
                {
                    boss.GetComponent<EnemyBoss>().Spawn();
                    boss.gameObject.SetActive(true);
                }

                _appearTime = Random.Range(MinAppearTime, MaxAppearTime);
                _accumTime = 0f;
            }
        }
        
        if (GameInput.GetDebugShipBoss())
        {
            var pool = PoolManager.Instance.GetPool("EnemyBoss");
            var boss = pool.GetItem();
            
            boss.GetComponent<EnemyBoss>().Spawn();
            boss.gameObject.SetActive(true);
        }
    }

}
