﻿using System.Collections;
using System.Collections.Generic;
using FrameLord.Sound;
using UnityEngine;

namespace SpaceInvaders
{
    public class EnemyBoss : Ship
    {
        private const float ShipPosY = 80f;
        
        enum Dir
        {
            LeftToRight,
            RightToLeft
        }

        private Dir _movingDir;

        // Sound channel id
        private int _sndChannelId;

        private float movSpeed = 50;
        
        /// <summary>
        /// Unity Awake Method
        /// </summary>
        protected new void Awake()
        {
            base.Awake();
            _state = State.Death;
        }

        new void Update()
        {
            base.Update();
            
            if (_state == State.Alive)
            {
                var dx = movSpeed * Time.deltaTime;
                
                if (_movingDir == Dir.LeftToRight)
                {
                    transform.position += new Vector3(dx, 0f, 0f);

                    if (transform.position.x > (GameManager.ScreenWidth >> 1) + 10f)
                    {
                        _state = State.Death;
                        SoundManager.Instance.StopSound(_sndChannelId);
                        
                        ReturnToPool();
                    }
                    
                }
                else
                {
                    transform.position -= new Vector3(dx, 0f, 0f);
                    
                    if (transform.position.x < -(GameManager.ScreenWidth >> 1) - 10f)
                    {
                        _state = State.Death;
                        SoundManager.Instance.StopSound(_sndChannelId);
                        
                        ReturnToPool();
                    }
                }
            }
        }
        
        public void Spawn()
        {
            _sprRnd.sprite = aliveSprites[0];
            _sprRnd.enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            
            _movingDir = (Dir) UnityEngine.Random.Range(0, 2);

            if (_movingDir == Dir.LeftToRight)
            {
                transform.position = new Vector3(-(GameManager.ScreenWidth >> 1) - 10f, ShipPosY, 0f);
            }
            else
            {
                transform.position = new Vector3((GameManager.ScreenWidth >> 1) + 10f, ShipPosY, 0f);
            }

            _sndChannelId = SoundManager.Instance.PlaySound("Ufo");

            // Set the mov speed depending on the level number
            movSpeed = 50f + 10f * GameManager.Instance.levelNum;
            
            _state = State.Alive;
        }
        
        protected override void OnHit()
        {
            SoundManager.Instance.StopSound(_sndChannelId);
        }
        
        protected override void OnDestroyed()
        {
            ReturnToPool();
        } 
    }
}