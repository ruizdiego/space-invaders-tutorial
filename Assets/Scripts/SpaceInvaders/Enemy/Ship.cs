﻿// .NET Framework
using System;

// Unity Framework
using UnityEngine;

// FrameLord
using FrameLord.Pool;
using FrameLord.Sound;

namespace SpaceInvaders
{
    public class Ship : PoolItem
    {
        private const float ExplosionFrameExposureTime = 0.5f;

        protected enum State
        {
            Alive,
            Dying,
            Death
        }

        // Ship type
        public ShipType shipType;
        
        // Alive sprites references
        public Sprite[] aliveSprites;

        // Explosion sprite reference
        public Sprite explosionSprite;

        // The pool of projectiles name
        public string ProjectilePoolName;

        // Shoot sound
        public string shootSound;
        
        // Hit sound
        public string hitSound;
        
        // Sprite Renderer component reference
        protected SpriteRenderer _sprRnd;

        // Frame mostrado actualmente
        protected int _curFrame;

        // Enenmy ship state
        protected State _state;

        // Time accumulator
        protected float _accumTime;
        
        // Is a projectile on air?
        private bool _projectileOnAir;
        
        /// <summary>
        /// Unity Awake Method
        /// </summary>
        protected void Awake()
        {
            _sprRnd = GetComponent<SpriteRenderer>();
            
            _state = State.Alive;

            _curFrame = 0;
            _sprRnd.sprite = aliveSprites[_curFrame];
        }

        /// <summary>
        /// Unity Update Method
        /// </summary>
        protected void Update()
        {
            switch (_state)
            {
                case State.Dying:
                    _accumTime += Time.deltaTime;
                    if (_accumTime > ExplosionFrameExposureTime)
                    {
                        _sprRnd.enabled = false;
                        _state = State.Death;

                        GameManager.Instance.NotifyShipDestruction(shipType);
                        
                        OnDestroyed();
                    }

                    break;
            }
        }

        /// <summary>
        /// Cambia el frame del sprite que se está visualizando
        /// </summary>
        public void ChangeFrame()
        {
            if (_state == State.Alive && aliveSprites.Length > 1)
            {
                _curFrame++;
                if (_curFrame == aliveSprites.Length) _curFrame = 0;
                _sprRnd.sprite = aliveSprites[_curFrame];
            }
        }

        /// <summary>
        /// Change the sprite color
        /// </summary>
        public void SetColor(Color c)
        {
            _sprRnd.color = c;
        }

        public void Die()
        {
            _accumTime = 0f;
            _state = State.Dying;
            _sprRnd.sprite = explosionSprite;

            // [Sound]
            SoundManager.Instance.PlaySound(hitSound);
            
            GameManager.Instance.NotifyShipHit(shipType);

            OnHit();
        }

        public void OnTriggerEnter2D(Collider2D col)
        {
            // If the collision is with a defense, return
            if (col.gameObject.GetComponent<Defense>()) return;
            
            // Alive and the other object should be different than me
            if (_state == State.Alive && string.Compare(col.gameObject.tag, transform.tag, StringComparison.Ordinal) != 0)
            {
                Die();

                // Disable the collider to avoid collisions with the explosion
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
        }

        /// <summary>
        /// La nave realiza un disparo.
        /// </summary>
        public void Shoot(Vector3 startPos)
        {
            var projectile = PoolManager.Instance.GetPool(ProjectilePoolName).GetItem();
            if (projectile != null)
            {
                // [Sound]
                SoundManager.Instance.PlaySound(shootSound);

                projectile.transform.position = startPos;
                projectile.gameObject.SetActive(true);
                projectile.GetComponent<Projectile>().SetOwner(this);

                _projectileOnAir = true;
            }
        }

        protected virtual void OnHit()
        {
        }
        
        protected virtual void OnDestroyed()
        {
        }

        /// <summary>
        /// Returns true if the ship is alive
        /// </summary>
        public bool IsAlive()
        {
            return _sprRnd.enabled;
        }

        protected bool CanShootAlready()
        {
            return !_projectileOnAir;
        }
        
        public void ProjectileHasExploded()
        {
            _projectileOnAir = false;
        }
    }
    
}