﻿// .NET Framework
using System;

// Unity Framework
using UnityEngine;

// FrameLord
using FrameLord.Pool;

namespace SpaceInvaders
{
    public class Projectile : PoolItem
    {
        enum State
        {
            Alive,
            Exploding,
            Exploded
        }
        
        // Margen a partir del cual se destruye o recicla el objeto
        private const float DestroyMarginValue = 0.85f;
        private const float ExplodeExpositionTime = 0.25f;
        private const float ProjectileAnimationFrameExpositionTime = 0.25f;

        // Velocidad de movimiento
        public float movSpeed = 200f;

        // Dirección de movimiento (1: Arriba, -1: Abajo)
        public int movDirY = 1;

        // Reference to the shoot default sprite
        public Sprite shootDefaultSprite;
        
        // Reference to the shoot explosion sprite
        public Sprite shootExplosionSprite;

        // Animate projectile by flipping
        public bool animateByFlipping = false;
        
        // Screen margin limit
        private float _screenMarginLimit;

        // State of the projectile
        private State _state;

        // Time accumulator
        private float _accumTime;

        // Ship owner reference
        private Ship _shipOwner;

        // Sprite renderer reference
        private SpriteRenderer _sprRnd;
        
        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            _sprRnd = GetComponent<SpriteRenderer>();
            _screenMarginLimit = (GameManager.ScreenHeight >> 1) * DestroyMarginValue * movDirY;
        }

        /// <summary>
        /// Unity Update Method
        /// </summary>
        void Update()
        {
            switch (_state)
            {
                case State.Alive:
                    var ypos = transform.position.y + movSpeed * Time.deltaTime * movDirY;

                    if (animateByFlipping)
                    {
                        _accumTime += Time.deltaTime;
                        if (_accumTime > ProjectileAnimationFrameExpositionTime)
                        {
                            _accumTime %= ProjectileAnimationFrameExpositionTime;
                            _sprRnd.flipX = !_sprRnd.flipX;
                        }
                    }
                    
                    if (Mathf.Abs(ypos) < Mathf.Abs(_screenMarginLimit))
                    {
                        transform.position = new Vector3(transform.position.x, ypos, transform.position.z);
                    }
                    else
                    {
                        Explode();
                    }
                    break;
                
                case State.Exploding:

                    _accumTime += Time.deltaTime;

                    if (_accumTime > ExplodeExpositionTime)
                    {
                        Destroy();
                    }
                    
                    break;
                
            }
        }

        public void OnTriggerEnter2D(Collider2D col)
        {
            if (((string.Compare(col.gameObject.tag, EnemyShip.Tag, StringComparison.Ordinal) == 0 && movDirY == 1) ||
                 (string.Compare(col.gameObject.tag, Player.Tag, StringComparison.Ordinal) == 0 && movDirY == -1)) &&
                 col.gameObject.GetComponent<Projectile>() == null)
            {
                Destroy();
            }
        }

        private void Explode()
        {
            _accumTime = 0f;
            _state = State.Exploding;
            
            _sprRnd.sprite = shootExplosionSprite;
        }

        public void Destroy()
        {
            // Return the projectile to the pool
            ReturnToPool();
                
            _state = State.Exploded;

            _shipOwner.ProjectileHasExploded();
       }
        
        /// <summary>
        /// Reset the sprite image
        /// </summary>
        public void SetOwner(Ship shipOwner)
        {
            _accumTime = 0f;
            _shipOwner = shipOwner;
            _state = State.Alive;
            if (_sprRnd != null) _sprRnd.sprite = shootDefaultSprite;
        }
    }
}