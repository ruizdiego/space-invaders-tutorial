﻿// Unity Framework

using System;
using UnityEngine;
using UnityEngine.SceneManagement;

// FrameLord 
using FrameLord.Core;
using FrameLord.EventDispatcher;
using FrameLord.FSM;

namespace SpaceInvaders
{
    public class GameManager : MonoBehaviorSingleton<GameManager>
    {
        public const int ScreenWidth = 224;
        public const int ScreenHeight = 256;
        
        public int initialNumOfLives = 3;

        // Points for the big ship enemy
        public int pointsForBigShip = 50;
        
        // Points for enemy 1
        public int pointsForEnemy1 = 30;
        
        // Points for enemy 2
        public int pointsForEnemy2 = 20;
        
        // Points for enemy 1
        public int pointsForEnemy3 = 10;

        // New life after points
        public int newLifeAfter = 1500;

        // Clear hi-score on editor in each run?
        public bool clearHiScoreOnEditor = false;

        // Level number
        public int levelNum = 1;
        
        // Current number of lives
        private int lives;

        // Player reference
        private Player player;

        [HideInInspector]
        // Player score
        public int score;

        // Reference to StateManager
        private StateManager _stateManager;
        
        /// <summary>
        /// Unity Start Method
        /// </summary>
        void Start()
        {
            _stateManager = GetComponent<StateManager>();
            
            if (Application.isEditor && clearHiScoreOnEditor)
            {
                HiscoreManager.Reset();
            }
            
            HiscoreManager.ReadHiScoreFromStorage();
            
            DontDestroyOnLoad(this);
        }

        void Update()
        {
            if (IsPlaying() && EnemyManager.Instance.AreAllShipsDestroyed())
            {
                levelNum++;
                
                // Load the action game scene
                SceneManager.LoadScene("SceneGame");
            }
        }
        
        /// <summary>
        /// Start game
        /// </summary>
        public void StartGame()
        {
        }
       
        /// <summary>
        /// Game reset
        /// </summary>
        public void Reset()
        {
            lives = initialNumOfLives;
            score = 0;
        }

        /// <summary>
        /// Game Over?
        /// </summary>
        public bool IsGameOver()
        {
            return lives == 0;
        }

        /// <summary>
        /// Returns true if the action game is playing
        /// </summary>
        public bool IsPlaying()
        {
            return string.Compare(_stateManager.GetCurrentStateId(), "ActionGame", StringComparison.Ordinal) == 0;
        }
        
        /// <summary>
        /// Returns the current number of lives
        /// </summary>
        public int GetLives()
        {
            return lives;
        }

        /// <summary>
        /// Respawn player
        /// </summary>
        public void RespawnPlayer()
        {
            if (player == null) player = GameObject.FindWithTag("Player").GetComponent<Player>();
            player.Respawn();
            
            GameEventDispatcher.Instance.Dispatch(this, new EvnPlayerRespawn());
        }
       
        /// <summary>
        /// A ship was hit. This function is called at the beginning of the destroy animation 
        /// </summary>
        public void NotifyShipHit(ShipType enemyType)
        {
            switch (enemyType)
            {
                case ShipType.EnemyType1:
                    GameEventDispatcher.Instance.Dispatch(this, new EvnScoreUpdate(pointsForEnemy1));
                    break;
                case ShipType.EnemyType2:
                    GameEventDispatcher.Instance.Dispatch(this, new EvnScoreUpdate(pointsForEnemy2));
                    break;
                case ShipType.EnemyType3:
                    GameEventDispatcher.Instance.Dispatch(this, new EvnScoreUpdate(pointsForEnemy3));
                    break;
                case ShipType.EnemyTypeBoss:
                    GameEventDispatcher.Instance.Dispatch(this, new EvnScoreUpdate(pointsForBigShip));
                    break;
            }
        }

        /// <summary>
        /// A ship was hit. This function is called at the end of the destroy animation 
        /// </summary>
        public void NotifyShipDestruction(ShipType enemyType)
        {
            switch (enemyType)
            {
                case ShipType.Player:
                    lives = Mathf.Max(lives - 1, 0);
                    GameEventDispatcher.Instance.Dispatch(this, new EvnPlayerDied());
                    break;
            }
        }

        /// <summary>
        /// The game is over
        /// </summary>
        public void NotifyGameOver()
        {
            HiscoreManager.WriteScoreToStorage();
        }
    }
}