﻿// Unity Framework
using UnityEngine;

// FrameLord
using FrameLord.FSM;

namespace SpaceInvaders
{
    public class ConIsGameOver : FrameLord.FSM.StateConnection
    {
        public float time;
        
        private float _accumTime;
        
        protected override void OnCheckCondition()
        {
            _accumTime += Time.deltaTime;
            _isFinished = (_accumTime >= time && GameManager.Instance.IsGameOver());
        }
        
        protected override void OnReset()
        {
            _accumTime = 0f;
        }
    }
}