﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
    public class ConGamePadButtonA : FrameLord.FSM.StateConnection
    {
        protected override void OnCheckCondition()
        {
            _isFinished = Input.GetKeyDown($"joystick button {FrameLord.Input.GamePad.GamepadButtonA}");
        }
    }
}