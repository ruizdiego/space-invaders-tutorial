﻿// Unity Framework

using FrameLord.Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceInvaders
{
    public class StateMainMenu : FrameLord.FSM.State
    {
        private int _sndId;
        
        protected override void OnEnterState()
        {
            SceneManager.LoadScene("SceneMainMenu");

            // [Sound]
            _sndId = SoundManager.Instance.FadeInSound("MainMenuMusic", 0.1f);
        }

        protected override void OnLeaveState()
        {
            SoundManager.Instance.FadeOutSound(_sndId, 0.1f);
            
            // Load the action game scene
            SceneManager.LoadScene("SceneGame");
            
            // Reset match
            GameManager.Instance.Reset();
        }
    }
}