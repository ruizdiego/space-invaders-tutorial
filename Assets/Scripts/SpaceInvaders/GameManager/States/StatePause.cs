﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
    public class StatePause : FrameLord.FSM.State
    {
        protected override void OnEnterState()
        {
            Time.timeScale = 0f;
        }
        
        protected override void OnLeaveState()
        {
            Time.timeScale = 1f;
        }
    }
}