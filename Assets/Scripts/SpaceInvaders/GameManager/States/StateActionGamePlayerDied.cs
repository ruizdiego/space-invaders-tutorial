﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceInvaders
{
    public class StateActionGamePlayerDied : FrameLord.FSM.State
    {
        protected override void OnLeaveState()
        {
            if (!GameManager.Instance.IsGameOver())
            {
                GameManager.Instance.RespawnPlayer();
            }
        }
    }
}