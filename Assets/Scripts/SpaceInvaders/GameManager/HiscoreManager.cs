﻿using System.Collections;
using System.Collections.Generic;
using SpaceInvaders;
using UnityEngine;

public class HiscoreManager : MonoBehaviour
{
    private const string HiScoreKey = "hiscore";
    
    public static int hiscore;
    
    /// <summary>
    /// Read the hi-score from storage
    /// </summary>
    public static void ReadHiScoreFromStorage()
    {
        hiscore = PlayerPrefs.GetInt(HiScoreKey, 0);
    }

    
    /// <summary>
    /// Write the hi-score to the storage
    /// </summary>
    public static void WriteScoreToStorage()
    {
        PlayerPrefs.SetInt(HiScoreKey, hiscore);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Reset the hi-score
    /// </summary>
    public static void Reset()
    {
        PlayerPrefs.SetInt(HiScoreKey, 0);
        PlayerPrefs.Save();
    }
}
